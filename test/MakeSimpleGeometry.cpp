#include "TGeoManager.h"
#include "TGeoVolume.h"
#include "TGeoMatrix.h"

void MakeSimpleGeom()
{
  TGeoManager *geom  = new TGeoManager("simple", "simple geometry");
  TGeoVolume *world  = geom->MakeBox("World", nullptr, 10, 10, 10);
  auto box           = geom->MakeBox("Module", nullptr, 2, 2, 10);
  TGeoRotation *rot1 = new TGeoRotation("rot1", 45, 0, 0);
  world->AddNode(box, 0, new TGeoIdentity());
  world->AddNode(box, 1, new TGeoCombiTrans(5, 5, 0, rot1));
  world->AddNode(box, 2, new TGeoCombiTrans(-5, 5, 0, rot1));
  world->AddNode(box, 3, new TGeoCombiTrans(-5, -5, 0, rot1));
  world->AddNode(box, 4, new TGeoCombiTrans(5, -5, 0, rot1));

  geom->SetTopVolume(world);
  geom->Export("SimpleGeom.root");
}

int main()
{
  MakeSimpleGeom();
  return 0;
}
