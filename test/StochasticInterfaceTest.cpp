#include "TGeoNode.h"
#include "TGeoManager.h"
#include "management/RootGeoManager.h" // from VecGeom -> load vecgeom geometry
#include "management/GeoManager.h"

#include <cmath>
#include "TGeoMaterial.h"
#include "TGeoNavigator.h"
#include "TMath.h"

#include "TGeoVecGeomNavigator.h"

// for point generation
#include "base/SOA3D.h"
#include "volumes/utilities/VolumeUtilities.h"

#include "base/Stopwatch.h"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>
#include <cstring>

using vecgeom::Vector3D;

TGeoNavigator *tgeonav;
TGeoVecGeomNavigator *nav;

bool ExpectApproxEqual(double x, double y)
{
  return std::abs(x - y) < 1E-5;
}

bool ExpectApproxEqualSafety(double x, double y)
{
  return (x > 0 && y > 0) || (x == 0. && y == 0.);
}

// compares points given as arrays
bool ComparePoint(double const *x, double const *y)
{
  return ExpectApproxEqual(x[0], y[0]) && ExpectApproxEqual(x[1], y[1]) && ExpectApproxEqual(x[2], y[2]);
}

template <typename T>
bool ComparePtr(T const *x, T const *y)
{
  return x == y;
}

template <typename T>
bool Compare(T x, T y)
{
  return x == y;
}

bool CompareString(const char *s1, const char *s2)
{
  return strcmp(s1, s2) == 0;
}

// checks all sorts of internal state consistencies
bool CheckState(TGeoNavigator *tgeonav, TGeoVecGeomNavigator *nav)
{
  assert(ComparePoint(tgeonav->GetCurrentPoint(), nav->GetCurrentPoint()));
  assert(ComparePoint(tgeonav->GetCurrentDirection(), nav->GetCurrentDirection()));
  // assert(ComparePoint(tgeonav->GetLastPoint(), nav->GetLastPoint()));

  assert(ExpectApproxEqual(tgeonav->GetStep(), nav->GetStep()));
  assert(Compare(tgeonav->IsOutside(), nav->IsOutside()));

  // protect: even if TGeo is outside it still has a valid GetCurrentNode
  if (!tgeonav->IsOutside()) {
    assert(ComparePtr(tgeonav->GetCurrentNode(), nav->GetCurrentNode()));
    assert(ComparePtr(tgeonav->GetNextNode(), nav->GetNextNode()));
    assert(ComparePtr(tgeonav->GetCurrentVolume(), nav->GetCurrentVolume()));
  }

  assert(Compare(tgeonav->IsOnBoundary(), nav->IsOnBoundary()));
  assert(ExpectApproxEqualSafety(tgeonav->GetSafeDistance(), nav->GetSafeDistance()));
  assert(ExpectApproxEqualSafety(tgeonav->GetLastSafety(), nav->GetLastSafety()));

  // this fails: GetLevel() might have different convention
  // assert(Compare(tgeonav->GetLevel(), nav->GetLevel()));

  // cmp path
  assert(CompareString(tgeonav->GetPath(), nav->GetPath()));

  // check some navigation method:
  // GetMother()
  if (tgeonav->GetLevel() > 1) {
    assert(ComparePtr(tgeonav->GetMother(), nav->GetMother()));
  }
  if (tgeonav->GetLevel() > 2) {
    assert(ComparePtr(tgeonav->GetMother(2), nav->GetMother(2)));
  }

  return true;
}

//// checks the GetMother interface
bool CheckIsSameLocation(TGeoNavigator *tgeonav, TGeoVecGeomNavigator *nav)
{
  if (!Compare(tgeonav->IsSameLocation(), nav->IsSameLocation())) {
    std::cerr << "Warning : IsSameLocation does not match " << tgeonav->IsSameLocation() << " " << nav->IsSameLocation()
              << "\n";
  }
  return true;
}

//// checks GetBranchNames()
// bool GetBranchName
//{

//}

// Test compatibility with the TGeo Interface via stochastic calls
// and subsequent
// make sure that we test each interface !!
void TestFindNode(Vector3D<double> const &globalpoint)
{
  tgeonav->Clear();
  nav->Clear();

  // locate
  tgeonav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  auto return1 = tgeonav->FindNode();
  //
  nav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  auto return2 = nav->FindNode();
  if (tgeonav->GetCurrentNode() != nav->GetCurrentNode()) {
    std::cerr << "problem in start state\n";
    return;
  }

  assert(return1 == return2);

  CheckState(tgeonav, nav);
}

// Find Node 2 times
void TestFindNode2Times(Vector3D<double> const &globalpoint)
{
  tgeonav->Clear();
  nav->Clear();

  // locate
  tgeonav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  tgeonav->FindNode();
  //
  nav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  nav->FindNode();
  if (tgeonav->GetCurrentNode() != nav->GetCurrentNode()) {
    std::cerr << "problem in start state\n";
    return;
  }

  // move point a bit
  tgeonav->SetCurrentPoint(globalpoint.x() + 0.1, globalpoint.y(), globalpoint.z());
  auto return1 = tgeonav->FindNode();
  nav->SetCurrentPoint(globalpoint.x() + 0.1, globalpoint.y(), globalpoint.z());
  auto return2 = nav->FindNode();
  assert(return1 == return2);
  CheckState(tgeonav, nav);
  CheckIsSameLocation(tgeonav, nav);
}

// Test the IsSameLocation interface without adjustment
void TestIsSameLocation(Vector3D<double> const &globalpoint)
{
  tgeonav->Clear();
  nav->Clear();

  // locate
  tgeonav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  tgeonav->FindNode();
  //
  nav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  nav->FindNode();

  // test IsSameLocation(x,y,z) interface
  bool issametgeo = tgeonav->IsSameLocation(globalpoint.x() + 10, globalpoint.y() + 10, globalpoint.z() + 10);
  bool issame     = nav->IsSameLocation(globalpoint.x() + 10, globalpoint.y() + 10, globalpoint.z() + 10);
  assert(issametgeo == issame);
  CheckState(tgeonav, nav);
}

// Test the IsSameLocation interface WITH adjustment
void TestIsSameLocationWithAdjustment(Vector3D<double> const &globalpoint)
{
  tgeonav->Clear();
  nav->Clear();

  // locate
  tgeonav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  tgeonav->FindNode();
  //
  nav->SetCurrentPoint(globalpoint.x(), globalpoint.y(), globalpoint.z());
  nav->FindNode();

  // test IsSameLocation(x,y,z) interface
  bool issametgeo = tgeonav->IsSameLocation(globalpoint.x() + 10, globalpoint.y() + 10, globalpoint.z() + 10, true);
  bool issame     = nav->IsSameLocation(globalpoint.x() + 10, globalpoint.y() + 10, globalpoint.z() + 10, true);
  assert(issametgeo == issame);
  CheckState(tgeonav, nav);
}

// Test compatibility with the TGeo Interface via stochastic calls
// and subsequent
// make sure that we test each interface !!
void TestFindBoundary(Vector3D<double> const &globalpoint, Vector3D<double> const &globaldir)
{
  tgeonav->ResetState();
  nav->Clear();

  // locate
  tgeonav->InitTrack(globalpoint.x(), globalpoint.y(), globalpoint.z(), globaldir.x(), globaldir.y(), globaldir.z());
  //
  nav->InitTrack(globalpoint.x(), globalpoint.y(), globalpoint.z(), globaldir.x(), globaldir.y(), globaldir.z());

  // navigate
  // TGeoNavigator needs to be passed a negative argument to enable correct preparation
  // for a subsequent FindNormalFast call
  auto return1 = tgeonav->FindNextBoundary(-1E30);
  auto return2 = nav->FindNextBoundary(-1E30);
  assert(return1 == return2);
  CheckState(tgeonav, nav);

  // take point close to boundary and calculate normal
  auto step = tgeonav->GetStep();
  tgeonav->SetCurrentPoint(globalpoint.x() + step * globaldir.x(), globalpoint.y() + step * globaldir.y(),
                           globalpoint.z() + step * globaldir.z());
  auto norm1 = tgeonav->FindNormalFast();

  nav->SetCurrentPoint(globalpoint.x() + step * globaldir.x(), globalpoint.y() + step * globaldir.y(),
                       globalpoint.z() + step * globaldir.z());
  auto norm2 = nav->FindNormalFast();

  //std::cerr << "\n";
  //std::cerr << norm1[0] << "\t" << norm1[1] << "\t" << norm1[2] << "\n";
  //std::cerr << norm2[0] << "\t" << norm2[1] << "\t" << norm2[2] << "\n";
  CheckState(tgeonav, nav);
  //std::cerr << "\n\n";
  assert(ComparePoint(norm1, norm2));
}

// Test compatibility with the TGeo Interface via stochastic calls
// and subsequent
// make sure that we test each interface !!
void TestFindBoundaryFiniteStep(Vector3D<double> const &globalpoint, Vector3D<double> const &globaldir)
{
  tgeonav->ResetState();
  nav->Clear();

  // locate
  tgeonav->InitTrack(globalpoint.x(), globalpoint.y(), globalpoint.z(), globaldir.x(), globaldir.y(), globaldir.z());
  //
  nav->InitTrack(globalpoint.x(), globalpoint.y(), globalpoint.z(), globaldir.x(), globaldir.y(), globaldir.z());

  double pstep{1.}; // a fixed but finite pstep

  // navigate
  tgeonav->FindNextBoundary(pstep);
  nav->FindNextBoundary(pstep);
  CheckState(tgeonav, nav);
}

int main(int argc, char *argv[])
{
  if (argc < 3){
    std::cerr << "Usage : StochasticInterfaceTest geometry.root VolumeName\n";
    return 1;
  }

  TGeoManager::Import(argv[1]);
  vecgeom::RootGeoManager::Instance().LoadRootGeometry();

  // init navigators
  tgeonav = gGeoManager->GetCurrentNavigator();
  nav     = new TGeoVecGeomNavigator();

  // generate arbitrary point pairs inside the detector (uses VecGeom)
  int np(10000);
  vecgeom::SOA3D<double> localpoints(np), globalpoints(np);
  vecgeom::SOA3D<double> globaldirections(np);
  auto lvol = vecgeom::GeoManager::Instance().FindLogicalVolume(argv[2]);
  if (lvol) {
    lvol->Print();
    vecgeom::volumeUtilities::FillGlobalPointsAndDirectionsForLogicalVolume<vecgeom::SOA3D<double>>(
        lvol, localpoints, globalpoints, globaldirections, 0.5, np);
  } else {
    return 1;
  }

  for (size_t i = 0; i < np; ++i) {
    TestFindNode(globalpoints[i]);
  }
  std::cerr << "FindNode passed\n";

  for (size_t i = 0; i < np; ++i) {
    TestFindNode2Times(globalpoints[i]);
  }
  std::cerr << "FindNode (twice) passed\n";

  for (size_t i = 0; i < np; ++i) {
    TestIsSameLocation(globalpoints[i]);
  }
  std::cerr << "TestIsSameLocation passed\n";

  for (size_t i = 0; i < np; ++i) {
    TestIsSameLocationWithAdjustment(globalpoints[i]);
  }
  std::cerr << "TestIsSameLocation With Adjustment passed\n";

  for (size_t i = 0; i < np; ++i) {
    TestFindBoundary(globalpoints[i], globaldirections[i]);
  }
  std::cerr << "FindBoundary passed\n";

  for (size_t i = 0; i < np; ++i) {
    TestFindBoundaryFiniteStep(globalpoints[i], globaldirections[i]);
  }
  std::cerr << "FindBoundary (finite step) passed\n";

  return 0;
}
