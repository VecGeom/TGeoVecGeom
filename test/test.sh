#!/bin/bash

# A list of test cases 

# from CMS
./StochasticInterfaceTest cms2015.root ZDC_EMLayer
./StochasticInterfaceTest cms2015.root MBWheel_1N
./StochasticInterfaceTest cms2015.root HVQX

# from simple ExN03
./StochasticInterfaceTest ExN03.root Lead
./StochasticInterfaceTest ExN03.root liquidArgon
./StochasticInterfaceTest ExN03.root World

# from the simp
./MakeSimpleGeometry
./StochasticInterfaceTest SimpleGeom.root World


