#include "TGeoVecGeomNavigator.h"

// from VecGeom
#include "management/RootGeoManager.h"
#include "navigation/NavigationState.h"
#include "navigation/VNavigator.h"
#include "base/Vector3D.h"

#include "navigation/VNavigator.h"
#include "navigation/GlobalLocator.h"
#include "navigation/NewSimpleNavigator.h"
#include "navigation/SimpleABBoxNavigator.h"
#include "navigation/SimpleABBoxLevelLocator.h"
#include "navigation/HybridNavigator2.h"

// from ROOT
#include "TGeoManager.h"

#include <stdexcept>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>

// anonymous namespace
namespace {

// a global helper method
TGeoNode *ToTGeo(vecgeom::VPlacedVolume const *pvol)
{
  return const_cast<TGeoNode *>(vecgeom::RootGeoManager::Instance().tgeonode(pvol));
}

void SwapStates(vecgeom::NavigationState *&s1, vecgeom::NavigationState *&s2)
{
  auto tmp = s1;
  s1       = s2;
  s2       = tmp;
}
}

void InitNavigators()
{
  using vecgeom::GeoManager;
  using vecgeom::SimpleABBoxNavigator;
  using vecgeom::HybridNavigator;
  using vecgeom::SimpleABBoxLevelLocator;
  using vecgeom::SimpleAssemblyAwareABBoxLevelLocator;
  using vecgeom::NewSimpleNavigator;

  for (auto &lvol : GeoManager::Instance().GetLogicalVolumesMap()) {
    if (lvol.second->GetDaughtersp()->size() < 4) {
      lvol.second->SetNavigator(NewSimpleNavigator<>::Instance());
    }
    if (lvol.second->GetDaughtersp()->size() >= 5) {
      lvol.second->SetNavigator(SimpleABBoxNavigator<>::Instance());
    }
    if (lvol.second->GetDaughtersp()->size() >= 10) {
      lvol.second->SetNavigator(HybridNavigator<>::Instance());
      vecgeom::HybridManager2::Instance().InitStructure((lvol.second));
    }

    if (lvol.second->ContainsAssembly()) {
      lvol.second->SetLevelLocator(SimpleAssemblyAwareABBoxLevelLocator::GetInstance());
    } else {
      lvol.second->SetLevelLocator(SimpleABBoxLevelLocator::GetInstance());
    }
  }
  std::cerr << "####### NAVIGATORS INITIALIZED ###########\n";
}

bool TGeoVecGeomNavigator::Setup()
{
  vecgeom::RootGeoManager::Instance().LoadRootGeometry();
  // create TGeoVecGeomNavigator

  // set a navigator
  using Navigator_t = TGeoVecGeomNavigator;

  auto navlist = gGeoManager->GetListOfNavigators();
  navlist->AddAt(new Navigator_t(gGeoManager), 0);
  gGeoManager->SetCurrentNavigator(0);
  auto nav = gGeoManager->GetCurrentNavigator();
  nav->BuildCache(true, false);
  nav->GetCache()->BuildInfoBranch();

  return true;
}

// constructor
void TGeoVecGeomNavigator::Init()
{
  if (!vecgeom::GeoManager::Instance().IsClosed()) {
    std::cerr << "Fatal error : VecGeom geometry not closed\n";
  }

  auto depth = vecgeom::GeoManager::Instance().getMaxDepth();
  fState     = vecgeom::NavigationState::MakeInstance(depth);
  fNewstate  = vecgeom::NavigationState::MakeInstance(depth);
  fCurrstate = vecgeom::NavigationState::MakeInstance(depth);

  // init the navigation states and other data members
  InitNavigators();
}

//
TGeoNode const *TGeoVecGeomNavigator::FindNode(bool)
{
  using vecgeom::Vector3D;
  using vecgeom::GeoManager;
  using namespace vecgeom::GlobalLocator;
  using vecgeom::RootGeoManager;
  const auto top = GeoManager::Instance().GetWorld();

  const auto lastNode = fCurrstate->Top();
  // clear the state before modifying
  fCurrstate->Clear();
  LocateGlobalPoint(top, fPoint, *fCurrstate, true);
  const auto newNode = fCurrstate->Top();
  fIsSameLocation    = lastNode == newNode;
  fCurrentNode       = ToTGeo(newNode);
  return fCurrentNode;
}

// (Double_t stepmax=TGeoShape::Big(), Bool_t compsafe=kFALSE)
TGeoNode *TGeoVecGeomNavigator::FindNextBoundaryAndStep(double stepmax, bool s)
{
  using vecgeom::Vector3D;
  using vecgeom::GeoManager;
  using vecgeom::VNavigator;

  // need a valid currnavstate

  // get navigator for this volume
  VNavigator const *nav = fCurrstate->Top()->GetLogicalVolume()->GetNavigator();
  // navigate

  // get current position and direction
  fStep = nav->ComputeStepAndPropagatedState(fPoint, fDirection, stepmax, *fCurrstate, *fNewstate);

  // This is the "Step" part

  // update states and points
  // update state
  SwapStates(fCurrstate, fNewstate);

  // update point
  fPoint       = fPoint + fStep * fDirection;
  fCurrentNode = ToTGeo(fCurrstate->Top());
  return fCurrentNode;
}

// (Double_t stepmax=TGeoShape::Big(), Bool_t compsafe=kFALSE)
TGeoNode *TGeoVecGeomNavigator::FindNextBoundary(double stepmax)
{
  using vecgeom::Vector3D;
  using vecgeom::GeoManager;
  using vecgeom::VNavigator;

  // check for negative stepmax: In TGeo this is used for configuration purpose
  // but VecGeom needs positive numbers in any case
  if (stepmax < 0.) stepmax = -stepmax;

  // get navigator for this volume
  VNavigator const *nav = fCurrstate->Top()->GetLogicalVolume()->GetNavigator();

  // get current position and direction
  fStep = nav->ComputeStep(fPoint, fDirection, stepmax, *fCurrstate, *fNewstate);

  // TGeo convention: if stepmax finite --> calculate safety also
  // The solution here is not optimal; We need to avoid calculating the coordinate transform multiple times
  // (Either add a combined ComputeStepAndSafety method
  // (or add a ComputeStepForLocalPoint method to VecGeom)

  if (stepmax < vecgeom::InfinityLength<double>()) {
    fSafety     = nav->GetSafetyEstimator()->ComputeSafety(fPoint, *fCurrstate);
    fLastSafety = fSafety;
  }

  // current node not modified

  // next node modified

  fNextNode = ToTGeo(fNewstate->Top());
  return fNextNode;
}

void TGeoVecGeomNavigator::LocalToMasterBomb(const Double_t *local, Double_t *master) const
{
  throw std::runtime_error("TGeoVecGeomNavigator::LocalToMasterBomb not implemented");
}

void TGeoVecGeomNavigator::MasterToLocalBomb(const Double_t *master, Double_t *local) const
{
  throw std::runtime_error("TGeoVecGeomNavigator::MasterToLocalBomb not implemented");
}

void TGeoVecGeomNavigator::GetBranchNames(Int_t * /*ids*/) const
{
  throw std::runtime_error("TGeoVecGeomNavigator::GetBranchNames not implemented");
}

void TGeoVecGeomNavigator::GetBranchNumbers(Int_t * /*copyNumbers*/, Int_t * /*volumeNumbers*/) const
{
  throw std::runtime_error("TGeoVecGeomNavigator::GetBranchNumbers not implemented");
}

const char *TGeoVecGeomNavigator::GetPath() const
{
  // TODO: make this more efficient
  std::stringstream s;
  if (!fCurrstate->IsOutside()) {
    fCurrstate->printVolumePath(s);
    fPath = std::string(s.str());
  } else {
    fPath = " ";
  }
  return fPath.c_str();
}

bool TGeoVecGeomNavigator::IsSameLocation(double x, double y, double z, bool updateState)
{
  // do some checks against safety etc.
  // something like (if new point within known safety do nothing ... )

  // actually call VecGeom functionality to
  // TODO: improve this by using some cached global matrix
  // TODO: update the currentpoint if flag asked for it
  bool same = vecgeom::GlobalLocator::HasSamePath(vecgeom::Vector3D<double>(x, y, z), *fCurrstate, *fNewstate);

// in Debug mode check if fNewState actually represents the state
#ifndef NDEBUG
  fState->Clear();
  const auto top = vecgeom::GeoManager::Instance().GetWorld();
  vecgeom::GlobalLocator::LocateGlobalPoint(top, vecgeom::Vector3D<double>(x, y, z), *fState, true);
  assert(fNewstate->Top() == fState->Top());
  assert(fNewstate->HasSamePathAsOther(*fState));
#endif
  if (updateState) {
    fPoint.Set(x, y, z);
    SwapStates(fCurrstate, fNewstate);
    fCurrentNode = ToTGeo(fCurrstate->Top());
    // do I need to update fNextNode??
  }
  return same;
}

void TGeoVecGeomNavigator::Clear()
{
  fCurrstate->Clear();
  fNewstate->Clear();
  fStep           = 0;
  fSafety         = 0;
  fIsSameLocation = false;
}

// computes safe distance for the current point
double Safety(bool inside)
{
  std::runtime_error("not implemented");
  return 0.;
}
